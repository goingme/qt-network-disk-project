#ifndef PRIVATECHAT_H
#define PRIVATECHAT_H

#include <QWidget>
#include"protocol.h"

namespace Ui {
class PrivateChat;
}

class PrivateChat : public QWidget
{
    Q_OBJECT

public:
    explicit PrivateChat(QWidget *parent = nullptr);
    ~PrivateChat();
    ///单例获取私聊
    static PrivateChat &getInstance();
    ///获取私聊对象
    void setChatName(QString strName);
    ///更新私聊消息
    void updateMsg(const PDU *pdu);


private slots:
    void on_sendMsg_pb_clicked();

private:
    Ui::PrivateChat *ui;
    ///记录聊天对象名
    QString m_strChatName;
    ///
    QString m_strLoginName;
};

#endif // PRIVATECHAT_H
