#ifndef SHAREFILE_H
#define SHAREFILE_H

#include <QWidget>
#include<QPushButton>   //按钮
#include<QVBoxLayout>   //垂直布局
#include<QButtonGroup>  //选择统一管理
#include<QScrollArea>   //区域选择
#include<QCheckBox> //处理打钩
#include<QListWidget>

class ShareFile : public QWidget
{
    Q_OBJECT
public:
    explicit ShareFile(QWidget *parent = nullptr);

    static ShareFile &getInstance();

    void test();

    void updateFriend(QListWidget *pFriendList);

signals:

public slots:
    void cancelSelect();
    void selectAll();
    void okShare();
    void cancleShare();

private:
    ///按钮 全选
    QPushButton *m_pSelectAllPB;
    ///按钮 取消所有选择
    QPushButton *m_pCancelSelectPB;

    ///按钮 确定
    QPushButton *m_pOKPB;
    ///按钮 取消此次分享
    QPushButton *m_pCancelPB;

    ///展示区域
    QScrollArea *m_pSA;
    ///要分享的好友
    QWidget *m_pFriendW;

    ///管理所有的好友
    QButtonGroup *m_pButtonGroup;
    QVBoxLayout *m_pFriendWVBL; //保存垂直布局
};

#endif // SHAREFILE_H
