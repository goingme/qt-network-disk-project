#include "sharefile.h"
#include"tcpclient.h"
#include"opewidget.h"

ShareFile::ShareFile(QWidget *parent) : QWidget(parent)
{
    m_pSelectAllPB=new QPushButton("全选");
    m_pCancelSelectPB=new QPushButton("取消选择");

    m_pOKPB=new QPushButton("确定");
    m_pCancelPB=new QPushButton("取消");

    m_pSA=new QScrollArea;
    m_pFriendW=new QWidget;

    m_pFriendWVBL=new QVBoxLayout(m_pFriendW);
    m_pButtonGroup=new QButtonGroup(m_pFriendW);
    m_pButtonGroup->setExclusive(false);    //使其能多选

    //水平布局合成  全选 取消选择
    QHBoxLayout *pTopHBL=new QHBoxLayout;
    pTopHBL->addWidget(m_pSelectAllPB);
    pTopHBL->addWidget(m_pCancelSelectPB);
    pTopHBL->addStretch();

    //水平布局合成  确定 取消
    QHBoxLayout *pDownHBL=new QHBoxLayout;
    pDownHBL->addWidget(m_pOKPB);
    pDownHBL->addWidget(m_pCancelPB);

    //垂直布局合成  显示区域放中间
    QVBoxLayout *pMainVBL=new QVBoxLayout;
    pMainVBL->addLayout(pTopHBL);
    pMainVBL->addWidget(m_pSA);
    pMainVBL->addLayout(pDownHBL);
    setLayout(pMainVBL);

    connect(m_pCancelPB,SIGNAL(clicked(bool)),this,SLOT(cancelSelect()));
    connect(m_pSelectAllPB,SIGNAL(clicked(bool)),this,SLOT(selectAll()));

    connect(m_pOKPB,SIGNAL(clicked(bool)),this,SLOT(okShare()));
    connect(m_pCancelPB,SIGNAL(clicked(bool)),this,SLOT(cancleShare()));

    //test();
}

ShareFile &ShareFile::getInstance()
{
    static ShareFile instance;
    return instance;
}

void ShareFile::test()
{
    QVBoxLayout *p=new QVBoxLayout(m_pFriendW);
    QCheckBox *pCB=NULL;
    for(int i=0;i<15;i++){
        pCB=new QCheckBox("jack");
        p->addWidget(pCB);
        //添加管理
        m_pButtonGroup->addButton(pCB);
    }
    m_pSA->setWidget(m_pFriendW);
}

void ShareFile::updateFriend(QListWidget *pFriendList)
{
    if(pFriendList==NULL){
        return;
    }
    //之前的好友列表
    QAbstractButton *tmp=NULL;
    QList<QAbstractButton*> preFriendList=m_pButtonGroup->buttons();
    //旧数据清除
    for(int i=0;i<preFriendList.size();i++){
        tmp=preFriendList[i];
        m_pFriendWVBL->removeWidget(tmp);
        m_pButtonGroup->removeButton(tmp);
        preFriendList.removeOne(tmp);
        delete tmp;
        tmp=NULL;
    }
    //新数据更新
    QCheckBox *pCB=NULL;
    for(int i=0;i<pFriendList->count();i++){
        pCB=new QCheckBox(pFriendList->item(i)->text());
        m_pFriendWVBL->addWidget(pCB);
        //添加管理
        m_pButtonGroup->addButton(pCB);
    }
    m_pSA->setWidget(m_pFriendW);
}

void ShareFile::cancelSelect()
{
    //获取勾选结果，全部取消勾选
    QList<QAbstractButton*> cbList=m_pButtonGroup->buttons();
    for(int i=0;i<cbList.size();i++){
        cbList[i]->setChecked(false);
    }
}

void ShareFile::selectAll()
{
    //获取勾选结果，全部取消勾选
    QList<QAbstractButton*> cbList=m_pButtonGroup->buttons();
    for(int i=0;i<cbList.size();i++){
        cbList[i]->setChecked(true);
    }
}

void ShareFile::okShare()
{
    //用户名 当前路径
    QString strName=TcpClient::getInstance().loginName();
    QString strCurPath=TcpClient::getInstance().curPath();
    QString strShareFileName=OpeWidget::getInstance().getBook()->getShareFileName();

    QString strPath=strCurPath+"/"+strShareFileName;

    //获得已勾选的人数
    QList<QAbstractButton*> cbList=m_pButtonGroup->buttons();
    int num=0;
    for(int i=0;i<cbList.size();i++){
        if(cbList[i]->isChecked()){
            num++;
        }
    }
    //caMsg写入名字，路径  caData写入发送方，人数
    PDU *pdu = mkPDU(32 * num + strPath.size() + 1);
    pdu->uiMsgType=ENUM_MSG_TYPE_SHARE_FILE_REQUEST;
    sprintf(pdu->caData,"%s %d",strName.toStdString().c_str(),num);
    int j=0;
    for(int i=0;i<cbList.size();i++){
        if(cbList[i]->isChecked()){
            memcpy((char*)(pdu->caMsg)+j*32,cbList[i]->text().toStdString().c_str(),cbList[i]->text().size());
            j++;
        }
    }
    memcpy((char*)(pdu->caMsg)+num*32,strPath.toStdString().c_str(),strPath.size());
    TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
    free(pdu);
    pdu=NULL;
}

void ShareFile::cancleShare()
{
    hide();
}
