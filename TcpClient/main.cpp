#include "tcpclient.h"
#include <QApplication>

//#include"sharefile.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

        //由于单例模式编写的TcpClient函数，不能按此方法构建
//    TcpClient w; //创建客户端
//    w.show();


    TcpClient::getInstance().show();

        //测试m_pButtonGroup窗口
//    ShareFile w;
//    w.show();

    return a.exec();
}
