#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QFile> //使用头文件
#include <QTcpSocket> //TCP协议头文件
#include "protocol.h" //协议数据单元
#include "opewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TcpClient; }
QT_END_NAMESPACE

class TcpClient : public QWidget
{
    Q_OBJECT

public:
    TcpClient(QWidget *parent = nullptr);
    ~TcpClient();
    void loadConfig();

    static TcpClient &getInstance();
    QTcpSocket &getTcpSocket();
    ///公有形式返回用户名
    QString loginName();
    ///返回当前路径
    QString curPath();
    ///更新当前路径
    void setCurPath(QString strCurPath);

public slots: //信号处理函数
    void showConnect();
    void recvMsg();

private slots:
    //void on_send_pb_clicked();

    void on_login_pb_clicked();

    void on_regist_pb_clicked();

    void on_cancle_pb_clicked();

private:
    Ui::TcpClient *ui;
    ///IP存放
    QString m_strIP;
    ///端口存放
    qint16 m_usPort;
    ///tcp协议对象
    QTcpSocket m_tcpSocket; //连接服务器，和服务器交互
    ///用户名
    QString m_strLoginName;

    ///定义路径
    QString m_strCurPath;
    QFile m_file;
};
#endif // TCPCLIENT_H
