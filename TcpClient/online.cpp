#include "online.h"
#include "ui_online.h"
#include<QDebug>
#include"tcpclient.h"

Online::Online(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Online)
{
    ui->setupUi(this);
}

Online::~Online()
{
    delete ui;
}

void Online::showUsr(PDU *pdu)
{
    if(pdu==NULL){
        return;
    }
    //数据提取
    uint uiSize=pdu->uiMsgLen/32;
    char caTmp[32];    //临时数组
    for(uint i=0;i<uiSize;i++){
        memcpy(caTmp,(char*)(pdu->caMsg)+i*32,32);
        qDebug()<<caTmp;
        ui->online_lw->addItem(caTmp);
    }
    //结束后pdu没有重置，会导致刷新后用户的重复添加
}

void Online::on_addFriend_pb_clicked()
{
    //获得选择的用户名
    QListWidgetItem *pItem=ui->online_lw->currentItem();
    QString strPersonName=pItem->text();
    //获取本用户名
    QString strLoginName=TcpClient::getInstance().loginName();
    //准备PDU信息，并发送
    PDU *pdu=mkPDU(0);
    pdu->uiMsgType=ENUM_MSG_TYPE_ADD_FRIEND_REQUEST;
    mempcpy(pdu->caData,strPersonName.toStdString().c_str(),strPersonName.size());
    mempcpy(pdu->caData+32,strLoginName.toStdString().c_str(),strLoginName.size());
    TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
    free(pdu);
    pdu=NULL;
}
