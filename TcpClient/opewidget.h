#ifndef OPEWIDGET_H
#define OPEWIDGET_H

#include <QWidget>
#include<QListWidget>
#include"friend.h"
#include"book.h"
#include<QStackedWidget>

class OpeWidget : public QWidget
{
    Q_OBJECT
public:
    ///OpeWidget初始化
    explicit OpeWidget(QWidget *parent = nullptr);
    ///OpeWidget单例模式
    static OpeWidget &getInstance();
    Friend *getFriend();
    Book *getBook();

signals:

public slots:

private:
    /// 操作列表
    QListWidget *m_pListW;
    /// 好友
    Friend *m_pFriend;
    /// 图书
    Book *m_pBook;

    ///窗口显示(堆栈)
    QStackedWidget *m_pSW;
};

#endif // OPEWIDGET_H
