#ifndef BOOK_H
#define BOOK_H

#include <QWidget>
#include <QListWidget>  //文件操作类
#include <QPushButton>  //文件移动
#include <QHBoxLayout>  //水平布局
#include <QVBoxLayout>  //垂直布局
#include "protocol.h"    //添加协议
#include <QTimer> //添加并行器

class Book : public QWidget
{
    Q_OBJECT
public:
    explicit Book(QWidget *parent = nullptr);
    ///更新列表
    void updateFileList(const PDU *pdu);
    void clearEnterDir();
    QString enterDir();
    void setDownloadFlag(bool status);
    bool getDownloadFlag();
    QString getSaveFilePath();
    QString getShareFileName();

    ///公有属性 总文件大小
    qint64 m_iTotal;
    ///公有属性 已收到多少
    qint64 m_iRecved;
signals:

public slots:
    void createDir();
    ///发送根据当前路径刷新文件目录的请求
    void flushFile();
    void delDir();
    void renameFile();
    void enterDir(const QModelIndex &index);
    void returnPre();
    void uploadFile();
    void uploadFileData();
    void delRegFile();
    void downloadFile();
    void shareFile();
    void moveFile();
    void selectDestDir();

private:
    ///文件列表
    QListWidget *m_pBookListW;

    ///（按钮）返回
    QPushButton *m_pReturnPB;
    ///（按钮）创建文件夹
    QPushButton *m_pCreateDirPB;
    ///删除文件夹
    QPushButton *m_pDelDirPB;
    ///重命名文件
    QPushButton *m_pRenamePB;
    ///刷新文件夹
    QPushButton *m_pFlushDirPB;

    ///上传文件
    QPushButton *m_pUploadPB;
    ///删除文件
    QPushButton *m_pDelFilePB;
    ///下载文件
    QPushButton *m_pDownloadPB;
    ///分享文件
    QPushButton *m_pShareFilePB;
    ///移动文件
    QPushButton *m_pMoveFilePB;
    ///选择文件夹
    QPushButton *m_SelectDirPB;

    ///进入后的目录
    QString m_strEnterDir;
    ///下载文件的路径
    QString m_strSaveFilePath;
    ///打开的路径
    QString m_strUploadFilePath;

    ///定时器
    QTimer *m_pTimer;
    ///检测是否出于下载状态
    bool m_bDownload;
    ///要分享的文件名
    QString m_strShareFileName;
    ///要移动的文件名
    QString m_strMoveFileName;
    QString m_strMoveFilePath;
    QString m_strDestDir;
};

#endif // BOOK_H
