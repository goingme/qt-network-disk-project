#include "friend.h"
#include "protocol.h"   //好友界面添加通讯协议
#include "tcpclient.h"  //需要通过socket发送
#include<QInputDialog>  //现成的对话框
#include <QDebug>
#include"privatechat.h"
#include<QMessageBox>

Friend::Friend(QWidget *parent) : QWidget(parent)
{
    m_pShowMsgTE=new QTextEdit;
    m_pFriendListWidget=new QListWidget;
    m_pInputMsgLE=new QLineEdit;

    m_pDelFriendPB=new QPushButton("删除好友");
    m_pFlushFriendPB=new QPushButton("刷新好友");
    m_pShowOnlineUsrPB=new QPushButton("显示在线用户");
    m_pSearchUsrPB=new QPushButton("查找用户");
    m_pMsgSendPB=new QPushButton("信息发送");
    m_pPrivateChatPB=new QPushButton("私聊");

    //水平布局 除信息发送的按钮
    QVBoxLayout *pRightPBVBL = new QVBoxLayout;
    pRightPBVBL->addWidget(m_pDelFriendPB);
    pRightPBVBL->addWidget(m_pFlushFriendPB);
    pRightPBVBL->addWidget(m_pShowOnlineUsrPB);
    pRightPBVBL->addWidget(m_pSearchUsrPB);
    pRightPBVBL->addWidget(m_pPrivateChatPB);

    //水平布局1 信息显示/好友列表/其余按钮
    QHBoxLayout *pTopHBL=new QHBoxLayout;
    pTopHBL->addWidget(m_pShowMsgTE);
    pTopHBL->addWidget(m_pFriendListWidget);
    pTopHBL->addLayout(pRightPBVBL);

    //水平布局2 窗口(信息输入)/按钮(信息发送)
    QHBoxLayout *pMsgHBL=new QHBoxLayout;
    pMsgHBL->addWidget(m_pInputMsgLE);
    pMsgHBL->addWidget(m_pMsgSendPB);

    m_pOnline =new Online;  //在线用户界面

    //合成垂直布局
    QVBoxLayout *pMain=new QVBoxLayout;
    pMain->addLayout(pTopHBL);
    pMain->addLayout(pMsgHBL);
    pMain->addWidget(m_pOnline);
    m_pOnline->hide();

    setLayout(pMain);

    //关联显示在线用户的信号槽
    connect(m_pShowOnlineUsrPB,SIGNAL(clicked(bool)),this,SLOT(showOnline()));
    //关联查找用户信息的信号槽
    connect(m_pSearchUsrPB,SIGNAL(clicked(bool)),this,SLOT(searchUsr()));
    //关联刷新好友信号槽
    connect(m_pFlushFriendPB,SIGNAL(clicked(bool)),this,SLOT(flushFriend()));
    //关联删除好友信号槽
    connect(m_pDelFriendPB, SIGNAL(clicked(bool)), this, SLOT(delFriend()));
    //关联私聊好友信号槽
    connect(m_pPrivateChatPB, SIGNAL(clicked(bool)), this, SLOT(privateChat()));
    //关联群聊信号槽
    connect(m_pMsgSendPB, SIGNAL(clicked(bool)), this, SLOT(groupChat()));

}

void Friend::showAllOnlineUsr(PDU *pdu)
{
    if(pdu==NULL){
        return;
    }
    m_pOnline->showUsr(pdu);
}

void Friend::updateFriendList(PDU *pdu)
{
    m_pFriendListWidget->clear();
    if(pdu==NULL){
        return;
    }
    uint uiSize=pdu->uiMsgLen/32;
    char caName[32]={'\0'};
    for(uint i=0;i<uiSize;i++){
        //数据拷贝，将其置入好友列表中
        memcpy(caName,(char*)(pdu->caMsg)+i*32,32);
        m_pFriendListWidget->addItem(caName);
    }
}

void Friend::updateGroupMsg(PDU *pdu)
{
    QString strMsg=QString("%1 says: %2").arg(pdu->caData).arg((char*)pdu->caMsg);
    m_pShowMsgTE->append(strMsg);
}

QListWidget *Friend::getFriendList()
{
    return m_pFriendListWidget;
}

void Friend::showOnline()
{
    if(m_pOnline->isHidden()){
        m_pOnline->show();

        //发送显示在线用户的请求
        PDU *pdu=mkPDU(0);
        pdu->uiMsgType=ENUM_MSG_TYPE_ALL_ONLINE_REQUEST;
        TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
        free(pdu);
        pdu=NULL;
    }
    else{
        m_pOnline->hide();
    }
}

void Friend::searchUsr()
{
    m_strSeachName=QInputDialog::getText(this,"搜索","用户名");
    if(!m_strSeachName.isEmpty()){
        qDebug()<<m_strSeachName;
        PDU *pdu=mkPDU(0);
        strncpy(pdu->caData,m_strSeachName.toStdString().c_str(),m_strSeachName.size());
        pdu->uiMsgType=ENUM_MSG_TYPE_SEARCH_USR_REQUEST;
        TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
        free(pdu);
        pdu=NULL;
    }
}

void Friend::flushFriend()
{
    QString strName=TcpClient::getInstance().loginName();   //用户名
    PDU *pdu=mkPDU(0);
    pdu->uiMsgType=ENUM_MSG_TYPE_FLUSH_FRIEND_REQUEST;
    memcpy(pdu->caData,strName.toStdString().c_str(),strName.size());
    TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
    free(pdu);
    pdu=NULL;
}

void Friend::delFriend()
{
    //从好友列表里选在线
    if(m_pFriendListWidget->currentItem()->text()!=NULL){
        //获取好友名
        QString strFriendName=m_pFriendListWidget->currentItem()->text();
        PDU *pdu=mkPDU(0);
        pdu->uiMsgType=ENUM_MSG_TYPE_DELETE_FRIEND_REQUEST;
        //用户名
        QString strSelfName=TcpClient::getInstance().loginName();
        memcpy(pdu->caData,strSelfName.toStdString().c_str(),strSelfName.size());
        memcpy(pdu->caData+32,strFriendName.toStdString().c_str(),strFriendName.size());
        TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
        free(pdu);
        pdu=NULL;
    }

}

void Friend::privateChat()
{
    //选择好友列表里，点到私聊按钮就显示
    if(m_pFriendListWidget->currentItem()->text()!=NULL){
        //获取私聊对象名 信息处理跳转到私聊窗口
        QString strChatName=m_pFriendListWidget->currentItem()->text();
        PrivateChat::getInstance().setChatName(strChatName);
        if (PrivateChat::getInstance().isHidden()){
            PrivateChat::getInstance().show();
        }
    }
    else{
        QMessageBox::warning(this,"私聊","请选择私聊对象");
    }
}

void Friend::groupChat()
{
    QString strMsg=m_pInputMsgLE->text();
    if(!strMsg.isEmpty()){
        PDU *pdu=mkPDU(strMsg.size()+1);
        pdu->uiMsgType=ENUM_MSG_TYPE_GROUP_CHAT_REQUEST;
        QString strName=TcpClient::getInstance().loginName();
        strncpy(pdu->caData,strName.toStdString().c_str(),strName.size());
        strncpy((char*)(pdu->caMsg),strMsg.toStdString().c_str(),strMsg.size());
        TcpClient::getInstance().getTcpSocket().write((char*)pdu,pdu->uiPDULen);
        free(pdu);
        pdu=NULL;
    }
    else{
        QMessageBox::warning(this,"群聊","信息不能为空");
    }
}
