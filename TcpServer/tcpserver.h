#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QFile> //使用头文件
#include "mytcpserver.h"

QT_BEGIN_NAMESPACE
namespace Ui { class TcpServer; }
QT_END_NAMESPACE

class TcpServer : public QWidget
{
    Q_OBJECT

public:
    TcpServer(QWidget *parent = nullptr);
    ~TcpServer();
    void loadConfig();

private:
    Ui::TcpServer *ui;
    ///IP存放
    QString m_strIP;
    ///端口存放
    qint16 m_usPort;
};
#endif // TCPSERVER_H
