#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QTcpServer>
#include<QList>
#include"mytcpsocket.h"

///建立后获取批量MyTcpSocket
class MyTcpServer : public QTcpServer
{
    Q_OBJECT
public:
    MyTcpServer();
    ///单例模式获取 MyTcpServer 对象
    static MyTcpServer &getInstance();
    ///重载客户端连接服务器的函数，将MyTcpSocket不断收集
    void incomingConnection(qintptr socketDescriptor);

    ///按名字查找socket转发
    void resend(const char* pername,PDU *pdu);

public slots:
    ///接收MyTcpSocket的下线信号，删除此Socket
    void deleteSocket(MyTcpSocket *mysocket);

private:
    QList<MyTcpSocket*> m_tcpSocketList;

};

#endif // MYTCPSERVER_H
