#include "tcpserver.h"
#include "ui_tcpserver.h"
#include <QByteArray> //字节数组，文件读取的返回值
#include <QDebug> //用于打印
#include <QMessageBox> //报错窗口
#include <QHostAddress> //用于此对象

///由TcpServer的构造函数主导
TcpServer::TcpServer(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::TcpServer)
{
    ui->setupUi(this);
    loadConfig();
    //服务器监听端口
    MyTcpServer::getInstance().listen(QHostAddress(m_strIP),m_usPort); //监听
}

TcpServer::~TcpServer()
{
    delete ui;
}

void TcpServer::loadConfig()
{
    QFile file(":/server.config"); //加载配置文件
    if(file.open(QIODevice::ReadOnly)){ //QIODevice为编写模式的作用域，返回值bool
        QByteArray baData = file.readAll(); //以字节数组形式返回
        QString strData = baData.toStdString().c_str(); //类型转化
        file.close();

        //数据处理
        strData.replace("\r\n"," "); //替换\r\n
        QStringList strList=strData.split(" "); //以空格切分出字符串列表
        m_strIP = strList.at(0);
        m_usPort = strList.at(1).toUShort();
        qDebug() << "ip:" << m_strIP << " port:" << m_usPort;
    }
    else{
        QMessageBox::critical(this,"open config","open config failed"); //报错--打开配置文件
    }
}
