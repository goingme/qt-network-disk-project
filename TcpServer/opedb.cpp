#include "opedb.h"
#include<QMessageBox>
#include<QDebug>

OpeDB::OpeDB(QObject *parent) : QObject(parent)
{
    m_db=QSqlDatabase::addDatabase("QSQLITE");
}

OpeDB &OpeDB::getInstance()
{
    static OpeDB instance;
    return instance;
}

void OpeDB::init()
{
    m_db.setHostName("localhost");
    m_db.setDatabaseName("D:\\work\\demo\\demo1-v1\\TcpServer\\cloud.db");
    if(m_db.open()){
        QSqlQuery query;
        query.exec("select * from usrInfo");
        while(query.next()){
            QString data=QString("%1,%2,%3").arg(query.value(0).toString()).arg(query.value(1).toString()).arg(query.value(2).toString());
            qDebug()<<data; //按照上一行的格式打印数据
        }
    }
    else{
        QMessageBox::critical(NULL,"打开数据库","打开数据库失败");
    }
}

OpeDB::~OpeDB()
{
    m_db.close();
}

bool OpeDB::handleRegist(const char *name, const char *pwd)
{
    if(name==NULL||pwd==NULL){
        qDebug()<<"name | pwd is NULL";
        return false;   //存在空数据，则无法注册
    }
    //注意存入的是字符串，要添加单引号
    QString data=QString("insert into usrInfo(name, pwd) values(\'%1\',\'%2\')").arg(name).arg(pwd);
    qDebug()<< data;
    QSqlQuery query;
    return query.exec(data); //返回能否注册
}

bool OpeDB::handleLogin(const char *name, const char *pwd)
{
    if(name==NULL||pwd==NULL){
        qDebug()<<"name | pwd is NULL";
        return false;   //存在空数据，则无法注册
    }
    //注意存入的是字符串，要添加单引号
    QString data=QString("select * from usrInfo where name=\'%1\' and pwd=\'%2\' and online=0").arg(name).arg(pwd);
    qDebug()<< data;
    QSqlQuery query;
    query.exec(data);
    //查找账户情况，若存在且登录状态为0，将其置为1
    if(query.next()){
        data=QString("update usrInfo set online=1 where name=\'%1\' and pwd=\'%2\' and online=0").arg(name).arg(pwd);
        qDebug()<< data;
        QSqlQuery query;
        query.exec(data);

        return true;
    }
    else{
        return false;
    }
}

void OpeDB::handleOffline(const char *name)
{
    if(name==NULL){
        qDebug()<<"name is NULL";
        return;
    }
    //命令行更新账户状态
    QString data=QString("update usrInfo set online=0 where name=\'%1\'").arg(name);
    //qDebug()<< data;
    QSqlQuery query;
    query.exec(data);
}

QStringList OpeDB::handleAllOnline()
{
    QString data=QString("select name from usrInfo where online=1");
    QSqlQuery query;
    query.exec(data);
    QStringList result;
    result.clear();

    while (query.next()) {
        result.append(query.value(0).toString());
    }
    return result;
}

int OpeDB::handleSearchUsr(const char *name)
{
    if(name==NULL){
        return -1;
    }
    QString data=QString("select online from usrInfo where name=\'%1\'").arg(name);
    QSqlQuery query;
    query.exec(data);
    if(query.next()){
        int ret=query.value(0).toInt();
        if(ret==1){
            return 1;   //对方在线
        }
        else if(ret==0){
            return 0;   //对方离线
        }
    }
    else{
        return -1;  //用户不存在
    }
}

int OpeDB::handleAddFriend(const char *pername, const char *name)
{
    if(pername==NULL||name==NULL){
        return -1;  //有信息为空
    }
    QString data=QString("select * from friend where (id=(select id from usrInfo where name=\'%1\') "
                         "and friendId=(select id from usrInfo where name=\'%2\')) "
                         "or (friendId=(select id from usrInfo where name=\'%3\') "
                         "and id=(select id from usrInfo where name=\'%4\'));"
                         "").arg(pername).arg(name).arg(pername).arg(name);
    qDebug()<<data;
    QSqlQuery query;
    query.exec(data);
    if(query.next()){
        return 0;   //对方已经是好友
    }
    else{
        QString data=QString("select online from usrInfo where name=\'%1\'").arg(pername);
        QSqlQuery query;
        query.exec(data);
        if(query.next()){
            int ret=query.value(0).toInt();
            if(ret==1){
                return 1;   //对方在线
            }
            else if(ret==0){
                return 2;   //对方离线
            }
        }
            return 3;  //用户不存在
    }
}

void OpeDB::handleAgreeAddFriend(const char *pername, const char *name)
{
    if (NULL == pername || NULL == name)
    {
        return;
    }
    //插入好友关系的数据 对方 请求者
    QString data = QString("insert into friend(id, friendId) values((select id from usrInfo where name=\'%1\'), (select id from usrInfo where name=\'%2\'))").arg(pername).arg(name);
    qDebug()<<data;
    QSqlQuery query;
    query.exec(data);
}

QStringList OpeDB::handleFlushFriend(const char *name)
{
    QStringList strFriendList;
    strFriendList.clear();
    if(name==NULL){
        return strFriendList;
    }

    QString data = QString("select name from usrInfo where online=1 and id in "
                           "(select id from friend where friendId="
                           "(select id from usrInfo where name=\'%1\'))"
                           "").arg(name);
    qDebug()<<data;
    QSqlQuery query;
    query.exec(data);
    while (query.next()) {
        strFriendList.append(query.value(0).toString());
        qDebug()<<query.value(0).toString();
    }

    data = QString("select name from usrInfo where online=1 and id in "
                   "(select friendId from friend where id="
                   "(select id from usrInfo where name=\'%1\'))"
                   "").arg(name);
    qDebug()<<data;
    query.exec(data);
    while (query.next()) {
        strFriendList.append(query.value(0).toString());
        qDebug()<<query.value(0).toString();
    }
    return strFriendList;
}

bool OpeDB::handleDelFriend(const char *name, const char *friendName)
{
    //信息源自好友列表，从好友关系中获取，所以删除的好友关系一定存在
    if(name==NULL||friendName==NULL){
        return false;
    }
    QString data = QString("delete from friend where id="
                           "(select id from usrInfo where name=\'%1\') and friendId="
                           "(select id from usrInfo where name=\'%2\')"
                           "").arg(name).arg(friendName);
    QSqlQuery query;
    qDebug()<<data;
    query.exec(data);

    data = QString("delete from friend where id="
                   "(select id from usrInfo where name=\'%1\') and friendId="
                   "(select id from usrInfo where name=\'%2\')"
                   "").arg(friendName).arg(name);
    qDebug()<<data;
    query.exec(data);
    return true;
}
