#ifndef OPEDB_H
#define OPEDB_H

#include <QObject>
#include<QSqlDatabase>  //连接数据库
#include<QSqlQuery>  //查询数据库
#include<QStringList>   //字符串列表

///
/// \brief 数据库类，内置m_db
///
class OpeDB : public QObject
{
    Q_OBJECT
public:
    explicit OpeDB(QObject *parent = nullptr);
    ///构造OpenDB
    static OpeDB& getInstance();
    ///打开数据库，成功则加载数据，失败则报错
    void init();
    ///关闭数据库
    ~OpeDB();

    ///注册处理，返回注册情况
    bool handleRegist(const char *name, const char *pwd);
    ///登录检查，返回检查结果
    bool handleLogin(const char *name, const char *pwd);
    ///注销账户处理
    void handleOffline(const char *name);
    ///以字符串列表返回查询在线用户结果
    QStringList handleAllOnline();
    ///查找用户信息 不存在，存在且在线，存在不在线
    int handleSearchUsr(const char *name);

    ///添加好友 对方/自己 已添加、在线、不在线
    int handleAddFriend(const char *pername,const char *name);
    ///同意添加好友 更新好友关系
    void handleAgreeAddFriend(const char *pername, const char *name);
    ///刷新好友列表 找到好友关系
    QStringList handleFlushFriend(const char *name);
    ///删除好友 自己 对方
    bool handleDelFriend(const char *name, const char *friendName);

signals:

public slots:
private:
    ///连接数据库
    QSqlDatabase m_db;
};

#endif // OPEDB_H
