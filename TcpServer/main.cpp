#include "tcpserver.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    OpeDB::getInstance().init();    //测试数据库能否正常连接

    TcpServer w;    //加载配置文件，由mytcpserver建立和地址的连接
    w.show();

    return a.exec();
}
