#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

#include<QTcpSocket>
#include"protocol.h"
#include"opedb.h"
#include<QDir>
#include<QFile>
#include<QTimer>

///
/// \brief MyTcpSocket连接信号，获取PDU数据单元,并显示
///
class MyTcpSocket : public QTcpSocket
{
    Q_OBJECT
public:
    ///连接信号
    MyTcpSocket();
    ///获取私有客户端名字
    QString getName();
    ///文件夹拷贝函数
    void copyDir(QString strSrcDir,QString strDestDir);
signals:
    void offline(MyTcpSocket *mysokect);
public slots:
    ///获取信息
    void recvMsg();
    ///处理客户端下线
    void clientOffline();
    ///发送文件
    void sendFileToClient();
private:
    ///记录客户端名字
    QString m_strName;

    ///处理接收文件
    QFile m_file;
    ///文件大小
    qint64 m_iTotal;
    ///文件接收大小
    qint64 m_iRecved;
    ///判断是否处于上传文件的状态
    bool m_bUploadt;

    QTimer *m_pTimer;
};

#endif // MYTCPSOCKET_H
