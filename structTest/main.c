#include <stdio.h>
#include <string.h>
///协议数据单元（protocol data unity）
typedef struct PDU{
    int a;
    int b;
    int c;
    ///没有大小的整形数组
    int d[];
}PDU;

int main()
{
    printf("%ld\n",sizeof(PDU));
    //将内存空间的首地址给PDU
    PDU *pdu = (PDU*)malloc(sizeof (PDU)+100*sizeof (int));
    pdu->a = 90;
    pdu->b = 89;
    pdu->c = 88;
    memcpy(pdu->d,"you jump i jump", 16);
    printf("a=%d,b=%d,c=%d,%s\n",pdu->a,pdu->b,pdu->c,(char*)(pdu->d));
    free(pdu);
    pdu = NULL;

    printf("Hello World!\n");
    return 0;
}
